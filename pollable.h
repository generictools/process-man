#ifndef _pollable_h
#define _pollable_h
#include <map>
#include <sys/epoll.h>

class EpollReactor;


class Pollable
{
public:
	/** Constructor
	 * @param fd_to_poll Filedescriptor to poll.
	 * @param op Flags for epoll; i.e. EPOLLIN | EPOLLOUT | EPOLLET.
	 */
	Pollable(int fd_to_poll, int op);
	
	/** Destructor
	 */
	~Pollable();
	
	/** Get the filedescriptor
	 */
	int get_fd();
	
	/** used by EpollReactor; returns a struct for epoll();
	 * @return The struct, initialized by the constructor.
	 */
	struct epoll_event * get_event_ptr();
	
	/** Register to Epollreactor for automatic de-registering.
	 * @param react EpollReactor to register to.
	 * */
	void registerReactor(EpollReactor * react);
	
	/** Called, when data becomes available on the filedescriptor.
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void readable();
	
	/** Called, when filedescriptor is ready to process more data.
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void writable();
	
	/** Called on error-conditions.
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void error();
	
	/** TODO: look up when this happens
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void pri();
	
	/** Called when remote part hangs up.
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void rdhup();
	
	/** Called when filedescriptor is closed.
	 * Virtual function to overwrite in derived classes.
	 */
	virtual void hup();
	
protected:
	EpollReactor * reactor;

private:
	int fd;
	int op;
	struct epoll_event event;
};

#endif
