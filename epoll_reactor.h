#ifndef _epoll_reactor_h
#define _epoll_reactor_h

#include <map>
#include <set>
#include "pollable.h"
#include <memory>

/** Uses epoll() to manage a number of Pollable-objects.
 * */
class EpollReactor
{
public:
	/** Constructor
	 */
	EpollReactor();
	
	/** Destructor
	 */
	~EpollReactor();
	
	/** start managing a Pollable
	 * @param p Pollable to take care of
	 */
	void registerPollable(std::shared_ptr<Pollable> p);
	
	/** remove Pollable by filedescriptor
	 * @param fd Filedescriptor to remove
	 */
	void deregister(int fd);
	
	/** wait a short period and handle Pollables-activity
	 */
	void wait_and_exec();
	
	/** get a shared pointer to a set of Pollables, where activity was
	 * monitored since the last wait_and_exec()-run.
	 * 
	 * @return shared pointer to a set of shared pointers to Pollables
	 * 	where activity was monitored since the last wait_and_exec()-run
	 */
	std::shared_ptr<std::set<std::shared_ptr<Pollable>>> getActivity();
	
private:
	std::map<int, std::shared_ptr<Pollable>> pollable_map;
	std::shared_ptr<std::set<std::shared_ptr<Pollable>>> activity;
	int epoll_fd;
	int events_per_iteration;
};

#endif
