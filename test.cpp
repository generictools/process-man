#include <iostream>
#include <string>

#include <cstring>

#include "process-man.h"
#include "epoll_reactor.h"



int main()
{
	std::list<std::function<void(void)>> callbacks;
	std::shared_ptr<Process> test_ls
		= std::make_shared<Process>(
			std::filesystem::path("/"),
			"/usr/bin/ls",
			std::list<std::string>{"-alh"});
			
	std::shared_ptr<Process> test_wc
		= std::make_shared<Process>(
			std::filesystem::path("/"),
			"/usr/bin/wc",
			std::list<std::string>{"-l"});
	
	test_ls->onOutStr(
		[test_wc](char * ptr, int size)
			{
				if(size == 0)
					return 0;
				std::shared_ptr<Buffer> newBuf
					= std::make_shared<Buffer>(size);
				std::memcpy(newBuf->ptr(), ptr, size);
				//std::cout << std::string(ptr, size);
				test_wc->putToStdin(newBuf);
				return 0;
			}
		);
	test_ls->onExit([test_wc, test_ls, &callbacks](unsigned char code)
		{
			std::cout << "ls Exited with code "
				<< (int) code << "." << std::endl;
			test_ls->closeFD(Process::Estdout);
			callbacks.push_back(
				[test_wc]()
				{
					test_wc->perform_io_in();
					test_wc->closeFD(Process::Estdin);
				});
			return 0;
		});
	
	test_wc->onOutStr(
		[](char * ptr, int size)
			{
				if(size == 0)
					return 0;
				std::string data(ptr, size);
				std::cout << data;
				std::cout.flush();
				return 0;
			}
		);
	test_wc->onExit([&callbacks](unsigned char code)
		{
			std::cout << "wc Exited with code "
				<< (int) code << "." << std::endl;
			return 0;
		});

	std::shared_ptr<ProcessManager> process_man
		= ProcessManager::create(4, 1024*1024);
		
	process_man->addProcess(test_ls, 100000);
	process_man->addProcess(test_wc, 100000);
	
	do
	{
		process_man->iterate();
		
		while(callbacks.begin() != callbacks.end())
		{
			(*callbacks.begin())();
			callbacks.pop_front();
		}
	} while (test_ls->is_running() || test_wc->is_running());
}
