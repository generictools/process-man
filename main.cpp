#include <iostream>
#include <string>

#include <cstring>

#include "process-man.h"
#include "epoll_reactor.h"



int main(int argc, char *argv[])
{
	long long mem_limit = 1000*1000;
	int proc_limit = 16;
	
	for(int i = 1; i < argc; i += 2)
	{
		if(argv[i][0] != '-')
			throw std::runtime_error("Parameter not recognized: " + std::string(argv[i]));
		char * ptr;
		switch(argv[i][1])
		{
		case 'p':
		case 'P':
			proc_limit = std::strtol(argv[i], &ptr, 10);
			break;
		case 'm':
		case 'M':
			mem_limit = std::strtoll(argv[i], &ptr, 10);
			break;
		}
	}
	
	std::list<std::function<void(void)>> callbacks;

	std::shared_ptr<ProcessManager> process_man
		= ProcessManager::create(proc_limit, mem_limit);
	
	do
	{
		if(process_man->reservedMemAvail() > 100000
			&& process_man->reservedProcessesAvail() > 0)
		{
			std::filesystem::path working_directory, command;
			std::list<std::string> args{};
			long long process_mem_consumption;
			
			std::cin >> process_mem_consumption;
			if(!std::cin.good())
				break;
			std::cin >> working_directory;
			if(!std::cin.good())
				break;
			std::cin >> command;
			if(!std::cin.good())
				break;
			
			std::string line;
			long long  pos, lastpos = 0;
			std::getline(std::cin, line);
			while((pos = line.find('\t'))  != std::string::npos)
			{
				std::string arg = line.substr(lastpos, pos-lastpos);
				lastpos = pos+1;
				std::cout << arg << std::endl;
				args.push_back(arg);
			}
			if(lastpos != 0)
			{
				std::string arg = line.substr(lastpos, std::string::npos);
				std::cout << arg << std::endl;
				args.push_back(arg);
			}
			std::cout << working_directory << ": "
				<< command << std::endl;
			std::shared_ptr<Process> new_process
				= std::make_shared<Process>(
					std::filesystem::path(working_directory),
					command,
					args);
			
			new_process->onOutStr(
				[new_process](char * ptr, int size)
					{
						if(size == 0)
							return 0;
						/*std::shared_ptr<Buffer> newBuf
							= std::make_shared<Buffer>(size);
						std::memcpy(newBuf->ptr(), ptr, size);
						new_process->putToStdin(newBuf);*/
						//std::cout << std::string(ptr, size);
						return 0;
					}
				);
			new_process->onExit([new_process, &callbacks](unsigned char code)
				{
					/*std::cout << "Exited with code "
						<< (int) code << "." << std::endl;*/
					new_process->closeFD(Process::Estdout);
					callbacks.push_back(
						[new_process]()
						{
							new_process->perform_io_in();
							new_process->closeFD(Process::Estdin);
							//new_process->closeFD(Process::Estdout);
							new_process->closeFD(Process::Estderr);
						});
					return 0;
				});
		
			process_man->addProcess(new_process, process_mem_consumption);
		}
		process_man->iterate();
		
		while(callbacks.begin() != callbacks.end())
		{
			(*callbacks.begin())();
			callbacks.pop_front();
		}
	} while (process_man->reservedProcessesAvail() < proc_limit
			&& std::cin.good());
	
}
