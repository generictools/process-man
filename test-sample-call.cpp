#include <iostream>
#include <string>

#include <cstring>

#include "process-man.h"
#include "epoll_reactor.h"

struct tesseract_workload
{
	std::filesystem::path wd;
	std::list<std::string> langs;
	std::list<std::string> input_files;
	std::string output_base;
	std::string config;
};



void tesseract_call(
	std::list<struct tesseract_workload> & work,
	std::shared_ptr<ProcessManager> & pm
	)
{
	for(auto & w : work)
	{
		std::stringstream arg_helper;
		for(auto & l : w.langs)
		{
			arg_helper << l << ",";
		}
		std::string lang_str(arg_helper.str());
		lang_str = lang_str.substr(0, lang_str.size()-1);
		
		std::shared_ptr<Process> p = std::make_shared<Process>(
				w.wd,
				"/usr/bin/tesseract",
				std::list<std::string>{"-l", lang_str, "-", w.output_base, "hocr"}
				);
		
		for(auto i : w.input_files)
		{
			std::shared_ptr<Buffer> b = std::make_shared<Buffer>(i.size()+2);
			std::memcpy(b->ptr(), i.c_str(), i.size());
			b->ptr()[i.size()+1] = '\n';
			b->ptr()[i.size()+2] = '\0';
			p->putToStdin(b);
		}
		p->onOutStr([](char * ptr, int n)
			{
				return 0;;
			});
		pm->addProcess(p, 10000);
	}
}

int main()
{
	std::shared_ptr<ProcessManager> process_man
		= ProcessManager::create(4, 1024*1024);
	
	struct tesseract_workload w1 = {
		"/",
		{"deu", "eng"},
		{"input1.jpg", "input2.jpg"},
		"meintest",
		"hocr"
		};
	struct tesseract_workload w2 = {
		"/",
		{"deu", "eng"},
		{"input1.jpg", "input2.jpg"},
		"meintest",
		"hocr"
		};
	struct tesseract_workload w3 = {
		"/",
		{"deu", "eng"},
		{"input1.jpg", "input2.jpg"},
		"meintest",
		"hocr"
		};
	std::list<tesseract_workload> test = {w1, w2, w3};
	tesseract_call(test, process_man);
	
	do
	{
		process_man->iterate();
	} while (process_man->reservedProcessesAvail() < 4);
}
